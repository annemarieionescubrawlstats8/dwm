#! /bin/bash
lxsession &
picom &
#nitrogen --restore &
dwmblocks &
#/usr/bin/emacs --daemon &
xset r rate 360 60
xrandr --output DVI-0 --set TearFree on
xranr --output DVI-0 --mode 1280x840_75.00
xrandr --output DVI-0 --set "scaling mode" "Center"
feh --randomize --bg-fill ~/Poze/wallpapers/Arch-2021-10-25-1635159311_screenshot_1280x840.jpg

### Uncomment only ONE of the following ###
# uncomment this line to restore last saved wallpaper...
#xargs xwallpaper --stretch < ~/.xwallpaper &
# ...or uncomment this line to set a random wallpaper on login
# find /usr/share/backgrounds/dtos-backgrounds/ -type f | shuf -n 1 | xargs xwallpaper --stretch &
